//// -- LEVEL 1
//// -- Tables and References

// Creating tables
Table tblog {
  id_log int(255) [pk]
  id_app int(255)
  user varchar(255)
  log int(255)
  subjek varchar(255)
}

Table tbkategori {
  id_kategori int(255) [pk]
  kategori varchar(255)
  aktif enum('on', 'off')
}

Table tbgambar {
  id_gambar int(255) [pk]
  id_app varchar(255)
  gambar varchar(255)
}

Table tbdownload {
  id_download int(255) [pk]
  id_app int(255)
  username varchar(255)
  tanggal date
}

Table tbapp {
  id_app int(255) [pk]
  id_jenis int(255)
  id_kategori int(255)
  judul varchar(255)
  username varchar(255)
  lihat varchar(255)
  download varchar(255)
  ukuran varchar(255)
  tanggal date
  deskripsi varchar(255)
  icon varchar(255)
  app varchar(255)
  header varchar(255)
  status enum('on', 'off')
}

Table komentar {
  id_komentar int(255) [pk]
  id_app int(255)
  username varchar(255)
  isi_komentar varchar(255)
  tgl date
  jam_komentar timestamp
  aktif enum('on', 'off')
}

Table users {
  username varchar(255) [pk]
  password varchar(255)
  nama_lengkap varchar(255)
  email varchar(255)
  url varchar(255)
  no_telp int(255)
  alamat varchar(255)
  level varchar(255)
  blokir varchar(255)
  tanggal date
  foto varchar(255)
  id_session int(255)
  com_code varchar(255)
}

Table tbjenis {
  id_jenis int(255) [pk]
  jenis varchar(255)
  jumlah float
}

Table vote {
  id_app int(255) [pk]
  counter varchar(255)
  value varchar(255)
  satu varchar(255)
  dua varchar(255)
  tiga varchar(255)
  empat varchar(255)
  lima varchar(255)
}

Ref: "tbapp"."id_app" < "tblog"."id_app"

Ref: "tbdownload"."id_app" < "tbapp"."id_app"

Ref: "tbkategori"."id_kategori" < "tbapp"."id_kategori"

Ref: "tbgambar"."id_app" < "tbapp"."id_app"

Ref: "komentar"."id_app" < "tbapp"."id_app"

Ref: "vote"."id_app" < "tbapp"."id_app"

Ref: "tbapp"."id_jenis" < "tbjenis"."id_jenis"

Ref: "tbapp"."username" < "users"."username"