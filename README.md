Pengertian JSON
JSON (JavaScript Object Notation) adalah format data yg biasanya digunakan untuk pertukaran dan penyimpanan data. Karena lebih sederhana dan lebih mudah dibaca, JSON biasanya digunakan sebagai format standar untuk bertukar data antar aplikasi.
JSON juga bisa dibaca banyak bahasa pemrograman seperti C, C++, C#, Java, Javascript Perl, Python, dll.

Struktur Dasar JSON

{
"Nama" : "Andhikus Elbarus Fahresus",
"Kelas" : "XII RPL"
}


Ini adalah contoh struktur JSON yg sederhana, dimulai dari kurung buka kurawal "{" dan ditutup dengan kurung tutup kurawal "}".
Di dalam kurung kurawal terdiri dari data data yg berisi format key dan isi nya.

Mengirim Array ke JSON dan Sebaliknya
Untuk mengirim Array ke JSON kita bisa menggunakan perintah json_encode()
sebaliknya jika kita ingin mengirim JSON ke Array kita menggunakan perintah json_decode()
contoh nya

<?php
$data = [
	'Nama' => $_POST['nama'],
	'Kelas' => $_POST['kelas']
];

$siswa = json_encode();

echo $siswa;
?>


sebaliknya

<?php
$data = [
	'Nama' => $_POST['nama'],
	'Kelas' => $_POST['kelas']
];

$siswa = json_decode();

echo $siswa;
?>



Contoh mengirim anuan ke anuan

<?php

$data = [
	'nama' => $_POST['nama'],
	'kelas' => $_POST['kelas']
];

$array = file_get_contents("siswa.txt");
$siswa = json_decode($array);

array_push($siswa, $data);

$masuk = json_encode($siswa);
file_put_contents("siswa.txt", $masuk);

print_r($masuk);

?>



<?php  
foreach ($siswa as $key => $value) { ?>
<tr>
	<td><?= $value['nama']; ?></td>
	<td><?= $value['kelas']; ?></td>
</tr>
<?php } ?>
