CREATE TABLE `tblog` (
  `id_log` int(255) PRIMARY KEY,
  `id_app` int(255),
  `user` varchar(255),
  `log` int(255),
  `subjek` varchar(255)
);

CREATE TABLE `tbkategori` (
  `id_kategori` int(255) PRIMARY KEY,
  `kategori` varchar(255),
  `aktif` enum
);

CREATE TABLE `tbgambar` (
  `id_gambar` int(255) PRIMARY KEY,
  `id_app` varchar(255),
  `gambar` varchar(255)
);

CREATE TABLE `tbdownload` (
  `id_download` int(255) PRIMARY KEY,
  `id_app` int(255),
  `username` varchar(255),
  `tanggal` date
);

CREATE TABLE `tbapp` (
  `id_app` int(255) PRIMARY KEY,
  `id_jenis` int(255),
  `id_kategori` int(255),
  `judul` varchar(255),
  `username` varchar(255),
  `lihat` varchar(255),
  `download` varchar(255),
  `ukuran` varchar(255),
  `tanggal` date,
  `deskripsi` varchar(255),
  `icon` varchar(255),
  `app` varchar(255),
  `header` varchar(255),
  `status` enum
);

CREATE TABLE `komentar` (
  `id_komentar` int(255) PRIMARY KEY,
  `id_app` int(255),
  `username` varchar(255),
  `isi_komentar` varchar(255),
  `tgl` date,
  `jam_komentar` timestamp,
  `aktif` enum
);

CREATE TABLE `users` (
  `username` varchar(255) PRIMARY KEY,
  `password` varchar(255),
  `nama_lengkap` varchar(255),
  `email` varchar(255),
  `url` varchar(255),
  `no_telp` int(255),
  `alamat` varchar(255),
  `level` varchar(255),
  `blokir` varchar(255),
  `tanggal` date,
  `foto` varchar(255),
  `id_session` int(255),
  `com_code` varchar(255)
);

CREATE TABLE `tbjenis` (
  `id_jenis` int(255) PRIMARY KEY,
  `jenis` varchar(255),
  `jumlah` float
);

CREATE TABLE `vote` (
  `id_app` int(255) PRIMARY KEY,
  `counter` varchar(255),
  `value` varchar(255),
  `satu` varchar(255),
  `dua` varchar(255),
  `tiga` varchar(255),
  `empat` varchar(255),
  `lima` varchar(255)
);

ALTER TABLE `tblog` ADD FOREIGN KEY (`id_app`) REFERENCES `tbapp` (`id_app`);

ALTER TABLE `tbapp` ADD FOREIGN KEY (`id_app`) REFERENCES `tbdownload` (`id_app`);

ALTER TABLE `tbapp` ADD FOREIGN KEY (`id_kategori`) REFERENCES `tbkategori` (`id_kategori`);

ALTER TABLE `tbapp` ADD FOREIGN KEY (`id_app`) REFERENCES `tbgambar` (`id_app`);

ALTER TABLE `tbapp` ADD FOREIGN KEY (`id_app`) REFERENCES `komentar` (`id_app`);

ALTER TABLE `tbapp` ADD FOREIGN KEY (`id_app`) REFERENCES `vote` (`id_app`);

ALTER TABLE `tbjenis` ADD FOREIGN KEY (`id_jenis`) REFERENCES `tbapp` (`id_jenis`);

ALTER TABLE `users` ADD FOREIGN KEY (`username`) REFERENCES `tbapp` (`username`);
